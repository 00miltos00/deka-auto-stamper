# !/usr/bin/python

"""
This is a Python script that adds the necessary signs and stamps on the certificate pdf files.
It uses the Python library PyPDF2.
Tested on Arch Linux with python 3.8.3 and PyPDF2 1.26.0-2

Miltiadis Vouzounaras 30/05/2020

example command -> deka-auto-stamper.py test.pdf spyros new

after run files -> test.pdf (the original unmodified file)
                   test_stamped.pdf (with sign and stamp)
                   test_stamped_copy.pdf (as above with copy watermark)

Default arguments are tasos new

filename  (Report.pdf)
name      (tasos, spyros, panagiotis, vasilis, miltos)
stamp     (new, old)
"""

import PyPDF2
import sys

das_input_file = 0
das_output_file = 0
das_output_file_copy = 0
das_sign_file = 0
das_stamp_file = 0
das_copy_file = "copy.pdf"

names = ["tasos", "spyros", "panagiotis", "vasilis", "miltos"]
stamps = ["new", "old"]

das_name = "tasos"
das_pages = 2
das_stamp = "new"

# Check for correct arguments
if len(sys.argv) < 2:
    raise ValueError("Not enough arguments, add at least one. Example deka-auto-stamper.py Report_2000000380.pdf")

if len(sys.argv) == 3:
    if sys.argv[2] in names:
        das_name = sys.argv[2]
    else:
        raise ValueError("Not valid name. Valid names are tasos, spyros, panagiotis, vasilis, miltos")

if len(sys.argv) == 4:
    if sys.argv[2] in names:
        das_name = sys.argv[2]
    else:
        raise ValueError("Not valid name. Valid names are tasos, spyros, panagiotis, vasilis, miltos")
    if sys.argv[3] in stamps:
        das_stamp = sys.argv[3]
    else:
        raise ValueError("Not Valid stamp. Valid stamps are new, old")

das_input_file = sys.argv[1]
das_output_file = das_input_file.replace(".pdf", "") + "_stamped.pdf"
das_output_file_copy = das_input_file.replace(".pdf", "") + "_stamped_copy.pdf"

das_sign_file = das_name + ".pdf"
das_stamp_file = das_stamp + ".pdf"

print("Input file is = \t", das_input_file)
print("Output file is =\t", das_output_file)
print("Copy file is =\t\t", das_output_file_copy)

print("name =\t\t\t", das_name)
# print("name file =\t\t", das_sign_file)
print("stamp =\t\t\t", das_stamp)
# print("stamp file =\t\t", das_stamp_file)
# print("Copy watermark file =\t", das_copy_file)


# opening all the needed files
with open(das_input_file, "rb") as filehandle_input:
    cert_pdf = PyPDF2.PdfFileReader(filehandle_input)
    das_pages = PyPDF2.PdfFileReader.getNumPages(cert_pdf)
    page_pdf = list(range(das_pages))
    page_copy_pdf = list(range(das_pages))

    print("This file has\t\t", das_pages, "pages")

    with open(das_sign_file, "rb") as filehandle_sign:
        cert_sign = PyPDF2.PdfFileReader(filehandle_sign)

        with open(das_stamp_file, "rb") as filehandle_stamp:
            cert_stamp = PyPDF2.PdfFileReader(filehandle_stamp)

            with open(das_copy_file, "rb") as filehandle_copy:
                cert_copy = PyPDF2.PdfFileReader(filehandle_copy)

                # Extract the first page for every file
                page_sign = cert_sign.getPage(0)
                page_stamp = cert_stamp.getPage(0)
                page_copy = cert_copy.getPage(0)

                for x in range(das_pages):
                    page_pdf[x] = cert_pdf.getPage(x)

                # Merge the sign page and the stamp page
                page_pdf[0].mergePage(page_sign)
                page_pdf[0].mergePage(page_stamp)

                output_cert = PyPDF2.PdfFileWriter()

                # Add all the pages for the final file
                for x in range(das_pages):
                    output_cert.addPage(page_pdf[x])

                # Export the final signed file
                with open(das_output_file, "wb") as filehandle_output:
                    output_cert.write(filehandle_output)

                # Merge the copy watermark on every page
                for x in range(das_pages):
                    page_copy_pdf[x] = cert_pdf.getPage(x)
                    page_copy_pdf[x].mergePage(page_copy)

                # Again, the sign page and the stamp page
                page_copy_pdf[0].mergePage(page_sign)
                page_copy_pdf[0].mergePage(page_stamp)

                output_cert_copy = PyPDF2.PdfFileWriter()

                # Add all the pages for the final file with copy watermark
                for x in range(das_pages):
                    output_cert_copy.addPage(page_copy_pdf[x])

                # Export the final signed and watermarked file
                with open(das_output_file_copy, "wb") as filehandle_output_copy:
                    output_cert_copy.write(filehandle_output_copy)
