# DEKA Auto Stamper

This Python script is using the PyPDF2 library to merge signs, stamp and a copy watermark on top of a .pdf file.

After the script execution, two new files are made:
	one with the choosen signature and stamp (_stamped.pdf)
	one as above but with an extra copy watermark (_stamped_copy.pdf)

The script can been execute with:
	no arguments	(default to tasos new)
	only name	(default to new)
	name and stamp

example command -> deka-auto-stamper.py test.pdf spyros old

after run files -> test.pdf (the original unmodified file)
                   test_stamped.pdf (with sign and stamp)
                   test_stamped_copy.pdf (as above with copy watermark)

filename  (.pdf)
name      (tasos, spyros, panagiotis, vasilis, miltos)
stamp     (new, old)

The premade signature .pdf files MUST be on the same folder with the script and the certificate.


This project has been created with:

Arch Linux https://www.archlinux.org/ as the operating system

Python (3.8.3-1) https://www.python.org/ as the programming language

PyPDF2 (1.26.0-2) https://pypi.org/project/PyPDF2/ as the library for manipulate the .pdf files

Pycharm (community-edition 2020.1.1-1) https://www.jetbrains.com/pycharm/ as the integrated development environment

Inkscape (1.0-4) https://inkscape.org/ as the vector graphics editor for the logo
